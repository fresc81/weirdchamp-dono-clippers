
# weirdchamp-dono-clippers

A tool to query the Twitch API for Clips.
It can be used to filter for clips that contain specific trigger words in the chat.
It can be used to track down users that clip donations which contain messages twitch chat reacts irritated to.

## Why

Some users donate via 3rd party integrations with the goal to have bigotted stuff being spoken on stream by
a text-to-speech program many broadcsters provide for their communities.
Other, not nessesarily the same but evenly, WeirdChamp users clip these messages with the goal of producing drama.
This tool can be used to find latter users.

## How

This tool queries the clips available on twitch and then sifts though the chat messages within those clips
to count the number of times the chat mentions specific emotes.

## Installation

This application requires [NodeJS](https://nodejs.org/).
After installing NodeJS you can use it's packet manager to install this application.

```
$ npm install -g https://gitlab.com/fresc81/weirdchamp-dono-clippers.git
```

### Create a Client ID

Head to your [Twitch Developer Console](https://dev.twitch.tv/console) and create an Application to obtain a Client ID and a Client Secret.

### Setup an Access Token

You can create your Twitch API Access Token with the [Twitch API Commandline Interface (CLI)](https://github.com/twitchdev/twitch-cli/releases).

```
$ twitch configure --client-id <your client id> --client-secret <your client secret>
$ twitch token
2021/08/15 01:23:45 App Access Token: <your access token>
```

This API token is usually valid for a month. If you get authentication errors just renew the token.

### Configure the application

Create an .env file containing at least your Client ID and the Access Token.
For example:

```
$ cat > .env
TWITCH_CLIENT_ID=<your client id>
TWITCH_API_TOKEN=<your access token>
[CTRL+D]
```

Refer to the [.env.example](.env.example) file for configuration details.

## Usage

```
usage:
  weirdchamp-dono-clippers [options]

description:
  fetches all clips created between start-time and end-time

options:
  --help                    print this help message
  --broadcaster=<channel>   channel name to fetch clips for
  --user=<nickname>         filter table for user nickname
  --from=<start-time>       the start-time in local time
  --from-utc=<start-time>   the start-time in UTC time
  --to=<end-time>           the end-time in local time
  --to-utc=<end-time>       the end-time in UTC time
  --format=(table|json)     specify output format
  --min-weirdchamps=<count> minimum number of weirdchamps
```
