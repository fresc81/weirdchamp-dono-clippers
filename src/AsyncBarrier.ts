import { setTimeoutPromise } from "./setTimeoutPromise";

/**
 * limits the number of concurrent accesses to a particular ressource
 */
export class AsyncBarrier {

    private numWaiting = 0;
    private numRunning = 0;

    constructor(
        public readonly maxConcurrent: number = 1,
        public readonly pollingTimeout: number = 50,
    ) {}

    public readonly getNumWaiting = () => this.numWaiting;

    public readonly getNumRunning = () => this.numRunning;

    public readonly enter = async () => {
        ++this.numWaiting;
        
        while (this.numRunning > this.maxConcurrent)
            await setTimeoutPromise(this.pollingTimeout);

        ++this.numRunning;
        --this.numWaiting;
    }

    public readonly leave = () => {
        --this.numRunning;
    }

}
