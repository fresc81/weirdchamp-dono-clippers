import { promisify } from "util";

// async setTimeout version
export const setTimeoutPromise = promisify((ms: number, cb: (err?: Error) => void) => {
    setTimeout(cb, ms);
});
