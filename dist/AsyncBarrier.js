"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AsyncBarrier = void 0;
const setTimeoutPromise_1 = require("./setTimeoutPromise");
/**
 * limits the number of concurrent accesses to a particular ressource
 */
class AsyncBarrier {
    constructor(maxConcurrent = 1, pollingTimeout = 50) {
        this.maxConcurrent = maxConcurrent;
        this.pollingTimeout = pollingTimeout;
        this.numWaiting = 0;
        this.numRunning = 0;
        this.getNumWaiting = () => this.numWaiting;
        this.getNumRunning = () => this.numRunning;
        this.enter = async () => {
            ++this.numWaiting;
            while (this.numRunning > this.maxConcurrent)
                await setTimeoutPromise_1.setTimeoutPromise(this.pollingTimeout);
            ++this.numRunning;
            --this.numWaiting;
        };
        this.leave = () => {
            --this.numRunning;
        };
    }
}
exports.AsyncBarrier = AsyncBarrier;
