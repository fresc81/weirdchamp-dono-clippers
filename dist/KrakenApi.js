"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KrakenApi = void 0;
const node_fetch_1 = __importDefault(require("node-fetch"));
const setTimeoutPromise_1 = require("./setTimeoutPromise");
const DEFAULT_BASE_URL = 'https://api.twitch.tv/kraken';
const API_REQUEST_TIMER_MS = 250;
/**
 * HypeE Kraken API
 */
class KrakenApi {
    /**
     * initialize the Kraken API object
     * @param clientId client id from the twitch console
     * @param apiToken api access token (can be created with the Twitch API CLI)
     * @param baseUrl api base URL
     */
    constructor(clientId, apiToken, baseUrl = DEFAULT_BASE_URL) {
        this.baseUrl = baseUrl;
        this.requestOptions = {
            headers: {
                'Accept': 'application/vnd.twitchtv.v5+json',
                'Authorization': `Bearer ${apiToken}`,
                'Client-id': clientId,
            }
        };
    }
    /**
     * gets chat messages from the (undocumented) twitch replay API,
     * allows to query for a particular time window
     * @param videoId the video id
     * @param offset offset in the video in seconds (defaults to start of stream)
     * @param length number of seconds to fetch (defaults to everything up to the end of the stream)
     * @returns an array of chat message objects
     */
    async getVideoComments(videoId, offset, length) {
        const start = offset || 0;
        const end = start + (length || (Number.MAX_SAFE_INTEGER - start));
        // make the first comments page URL
        const firstPageUrl = () => `${this.baseUrl}/videos/${videoId}/comments?content_offset_seconds=${start}`;
        // make the next comments page URL
        const nextPageUrl = (cursor) => `${this.baseUrl}/videos/${videoId}/comments?cursor=${cursor}`;
        const collectedComments = [];
        // fetch as many pages as needed to fulfill the request...
        let pageUrl = firstPageUrl();
        for (let i = 0; i < Number.MAX_SAFE_INTEGER; i++) {
            // make request and retrieve JSON
            const response = await node_fetch_1.default(pageUrl, this.requestOptions);
            if (!response.ok)
                throw new Error(`ApiRequestError: ${pageUrl} ${response.statusText} (${response.status})`);
            const responseJson = await response.json();
            // get comments and the pointer to the next page
            const { comments, _next } = responseJson;
            let lastMessageOffset = 0;
            // collect relevant data
            collectedComments
                .push(...comments
                // filter messages between start and end time
                .filter(comment => {
                lastMessageOffset = comment.content_offset_seconds;
                return (comment.content_offset_seconds >= start) && (comment.content_offset_seconds <= end);
            })
                // only selected columns
                .map(comment => ({
                created_at: comment.created_at,
                commenter: comment.commenter.display_name,
                offset: comment.content_offset_seconds,
                message: comment.message.body,
            })));
            // prepare URL for next page
            if (!_next || (lastMessageOffset > end))
                break;
            pageUrl = nextPageUrl(_next);
            // wait some time before next request
            await setTimeoutPromise_1.setTimeoutPromise(API_REQUEST_TIMER_MS);
        }
        return collectedComments;
    }
    /**
     * fetch clip metadata
     * @param slug the clip id
     * @param fetchMessages set to true if clip messages should be fetched
     * @returns the clip's metadata object
     */
    async getClip(slug, fetchMessages = false) {
        const pageUrl = `${this.baseUrl}/clips/${slug}`;
        const response = await node_fetch_1.default(pageUrl, this.requestOptions);
        if (!response.ok)
            throw new Error(`ApiRequestError: ${pageUrl} ${response.statusText} (${response.status})`);
        const responseJson = await response.json();
        if (fetchMessages) {
            const { duration } = responseJson;
            if (responseJson.vod) {
                const { id, offset } = responseJson.vod;
                responseJson.messages = await this.getVideoComments(id, offset, duration);
            }
            else {
                responseJson.messages = [];
            }
        }
        return responseJson;
    }
    /**
     * uses the messages from the clip to determine it's starting time
     * @param krakenClip the clip to calculate the start time from
     * @returns the clip's starting time
     */
    getClipStartTime(krakenClip) {
        const { messages } = krakenClip;
        if (!messages || !Array.isArray(messages))
            return null;
        return messages.reduce((prev, curr) => {
            const current = new Date(curr.created_at);
            if (prev === null)
                return current;
            if (current.valueOf() < prev.valueOf())
                return current;
            return prev;
        }, null);
    }
    /**
     * uses the messages from the clip to determine it's ending time
     * @param krakenClip the clip to calculate the end time from
     * @returns the clip's ending time
     */
    getClipEndTime(krakenClip) {
        const { messages } = krakenClip;
        if (!messages || !Array.isArray(messages))
            return null;
        return messages.reduce((prev, curr) => {
            const current = new Date(curr.created_at);
            if (prev === null)
                return current;
            if (current.valueOf() > prev.valueOf())
                return current;
            return prev;
        }, null);
    }
}
exports.KrakenApi = KrakenApi;
