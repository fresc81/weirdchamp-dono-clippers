"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setTimeoutPromise = void 0;
const util_1 = require("util");
// async setTimeout version
exports.setTimeoutPromise = util_1.promisify((ms, cb) => {
    setTimeout(cb, ms);
});
