"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
const auth_1 = require("@twurple/auth");
const api_1 = require("@twurple/api");
const KrakenApi_1 = require("./KrakenApi");
const AsyncBarrier_1 = require("./AsyncBarrier");
// pickup .env configuration
dotenv_1.config();
const clientId = process.env.TWITCH_CLIENT_ID || '';
const accessToken = process.env.TWITCH_API_TOKEN || '';
const broadcasterName = process.env.TWITCH_BROADCASTER || '';
const apiMaxConcurrency = Number.parseInt(process.env.API_MAX_CONCURRENCY || '3');
const apiPollingTimeout = Number.parseInt(process.env.API_POLLING_TIMEOUT || '500');
const weirdChampWords = (process.env.WEIRDCHAMP_WORDS || 'WeirdChamp PogO cmonBruh').trim().split(/\s+/);
// initialize authentication
const authProvider = new auth_1.StaticAuthProvider(clientId, accessToken);
// initialize Twitch API client
const apiConfig = {
    authProvider,
    logger: {
        name: 'twitch-api',
        colors: true,
        emoji: true,
        minLevel: 3,
        timestamps: true
    }
};
const apiClient = new api_1.ApiClient(apiConfig);
const krakenApi = new KrakenApi_1.KrakenApi(clientId, accessToken);
const currentTime = new Date();
const oneHourEarlier = new Date(currentTime.getTime() - (1000 * 60 * 60));
/**
 * parsed program arguments
 */
const programOptions = {
    broadcaster: broadcasterName,
    userName: null,
    fromUtc: oneHourEarlier,
    toUtc: currentTime,
    format: 'table',
    minWeirdchamps: 0,
};
/**
 * checks if a word is an weirdchamp emote
 * @param word the word to match against the list of weirdchamp emotes
 * @returns true if an weirdchamp emote
 */
function containedInWeirdchamps(word) {
    return weirdChampWords.some(weirdchampWord => word.toLowerCase() === weirdchampWord.toLowerCase());
}
/**
 * counts how often the message mentions one of the weirdchamp emotes
 * @param msg the message to count weirdchamps in
 * @returns the number of weirdchamp emotes
 */
function countWeirdchampsInMessage(msg) {
    const words = msg.message.trim().split(/\s/);
    return words.reduce((sum, word) => sum + (containedInWeirdchamps(word) ? 1 : 0), 0);
}
/**
 * counts how often the clip's messages mention one of the weirdchamp emotes
 * @param clip the clip to count the emotes for
 * @returns the number of weirdchamp (and related) emotes
 */
function countClipWeirdChampMessages(krakenClip) {
    const messages = krakenClip.messages;
    return messages.reduce((prev, curr) => prev + countWeirdchampsInMessage(curr), 0);
}
/**
 * counts the number of messages by a particular user
 * @param krakenClip the clip to count the messages for
 * @param userName the username to look up
 * @returns number of messages by userName
 */
function countClipMessagesByUser(krakenClip, userName) {
    const messages = krakenClip.messages;
    return messages.reduce((prev, curr) => prev + (curr.commenter === userName ? 1 : 0), 0);
}
/**
 *
 * @param clip
 * @param asyncBarrier
 * @returns
 */
async function getKrakenClip(clip, asyncBarrier) {
    const slug = clip.id;
    await asyncBarrier.enter();
    const krakenClip = await krakenApi.getClip(slug, true);
    asyncBarrier.leave();
    return krakenClip;
}
/**
 * performs the given paginated API request by retrieving all pages and printing them to the console
 * @param paginatedClipsRequest the paginated API request
 */
async function outputAllPages(paginatedClipsRequest) {
    // retrieve all pages
    const clips = await paginatedClipsRequest.getAll();
    // console.debug(`number of clips fetched: ${clips.length}`);
    // used to limit number of concurrent API requests
    const asyncBarrier = new AsyncBarrier_1.AsyncBarrier(apiMaxConcurrency, apiPollingTimeout);
    // select relevant data
    const tableDataPromises = clips
        // filter for a particular user
        .filter(clip => programOptions.userName ? clip.creatorDisplayName.toLowerCase() === programOptions.userName.toLowerCase() : true)
        // select table columns
        .map(async (clip) => {
        const krakenClip = await getKrakenClip(clip, asyncBarrier);
        return {
            startTime: krakenApi.getClipStartTime(krakenClip),
            endTime: krakenApi.getClipEndTime(krakenClip),
            creationDate: clip.creationDate,
            creatorDisplayName: clip.creatorDisplayName,
            url: clip.url,
            views: clip.views,
            videoId: clip.videoId,
            clipperMessageCount: countClipMessagesByUser(krakenClip, clip.creatorDisplayName),
            weirdChampCount: countClipWeirdChampMessages(krakenClip)
        };
    });
    const tableData = (await Promise.all(tableDataPromises))
        // filter for weirdchamp treshold
        .filter(row => row.weirdChampCount >= programOptions.minWeirdchamps)
        // sort by clip start time
        .sort((a, b) => (a.startTime?.valueOf() || a.creationDate.valueOf()) - (b.startTime?.valueOf() || b.creationDate.valueOf()));
    switch (programOptions.format) {
        case 'table':
            // output a fancy table
            console.table(tableData);
            break;
        case 'json':
            // output a JSON array
            console.log(JSON.stringify(tableData, null, 2));
            break;
    }
}
/**
 * async main application entry point
 */
async function main() {
    // resolve the broadcaster id from the boadcaster's channel name
    const broadcaster = await apiClient.users.getUserByName(programOptions.broadcaster);
    const broadcasterId = broadcaster?.id;
    if (!broadcasterId)
        throw new Error("broadcaster not found");
    //console.debug(`broadcaster: ${broadcaster?.displayName} (${broadcasterId})`);
    // specify the time in between which all clipped events should be retrieved
    const startDate = programOptions.fromUtc.toISOString(); // note: provide UTC time - retrieved clips are in local time
    const endDate = programOptions.toUtc.toISOString();
    //console.debug(`fetching clips for ${broadcaster?.displayName} between ${programOptions.fromUtc.toLocaleString()} and ${programOptions.toUtc.toLocaleString()}...`);
    // create a Twitch API request that fetches all clips produced between startDate and endDate
    const paginatedClipsRequest = apiClient.clips.getClipsForBroadcasterPaginated(broadcasterId, { startDate, endDate });
    await outputAllPages(paginatedClipsRequest);
}
/**
 * print application usage
 */
function printHelp() {
    const log = console.info;
    log('usage:');
    log('  weirdchamp-dono-clippers [options]');
    log();
    log('description:');
    log('  fetches all clips created between start-time and end-time');
    log();
    log('options:');
    log('  --help                    print this help message');
    log('  --broadcaster=<channel>   channel name to fetch clips for');
    log('  --user=<nickname>         filter table for user nickname');
    log('  --from=<start-time>       the start-time in local time');
    log('  --from-utc=<start-time>   the start-time in UTC time');
    log('  --to=<end-time>           the end-time in local time');
    log('  --to-utc=<end-time>       the end-time in UTC time');
    log('  --format=(table|json)     specify output format');
    log('  --min-weirdchamps=<count> minimum number of weirdchamps');
    log();
}
/**
 * processes a given commandline argument
 * @param arg commandline argument in '--key=value' format
 */
function processArg(arg) {
    const [key, value] = arg.split('=');
    switch (key) {
        case '--broadcaster': // channel to fetch clips for
            programOptions.broadcaster = value;
            break;
        case '--user': // filter result list for particular user
            programOptions.userName = value;
            break;
        case '--min-weirdchamps': // filter result list for particular user
            programOptions.minWeirdchamps = Number.parseInt(value);
            break;
        case '--from': // start-time given in local time
            programOptions.fromUtc = new Date(Date.parse(value));
            break;
        case '--from-utc': // start-time given in UTC
            programOptions.fromUtc = new Date(value);
            break;
        case '--to': // end-time given in local time
            programOptions.toUtc = new Date(Date.parse(value));
            break;
        case '--to-utc': // end-time given in UTC
            programOptions.toUtc = new Date(value);
            break;
        case '--format':
            switch (value) {
                case 'table':
                case 'json':
                    programOptions.format = value;
                    break;
                default:
                    printHelp();
                    process.exit(0);
            }
            break;
        case '--help':
        default:
            printHelp();
            process.exit(0);
    }
}
// process commandline arguments
process.argv.slice(2).forEach(arg => processArg(arg));
// start application asyncronously
main().catch(err => console.error("ERROR:", err));
